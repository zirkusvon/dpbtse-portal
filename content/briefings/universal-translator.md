---
title: "Universal Translator and Transformer"
date: 2024-05-23T00:00:00+01:00
publishdate: 2024-05-23T00:00:00+01:00
image: "/img/posts/briefings.jpg"
tags: ["briefing"]
type: "post"
summary: "Improve the real-time universal text translator and transformer
for individuals and organizations to deconstruct texts into
neutral core meanings and reconstruct them in various culturally and
emotionally diverse representations with enhanced understanding,
reduced polarization, and improved communication across different
perspectives."
comments: false
---

**Design challenge:**

> Improve the **real-time universal text translator and transformer**
> for **individuals and organizations** to **deconstruct texts into
> neutral core meanings and reconstruct them in various culturally and
> emotionally diverse representations** with **enhanced understanding,
> reduced polarization, and improved communication across different
> perspectives**.

The challenge can be approached from different perspectives and can be
adjusted to the IT Area of interest of the student with an interest in:

**HBO-i domains:** Software Engineering, User Interaction\
**IT-Areas:** AI engineering, web development (front-end/back-end), UX
design

## Context {#context-1}

The research group Interaction Design (IXD) is interested in the shape,
matter, and power of our relationship with information. In particular,
we see how humans struggle in the face of information overload:

-   Quantity of information (too much or not enough),
-   Quality of information (information density, ease of use of info),
-   Complexity or Simplicity of information (too much or too little).

This struggle is not new ([Kierkegaard,
1840](https://www.historynewsnetwork.org/article/how-to-cope-with-information-overload)),
but the technologies surrounding us are. Do those new technologies and
the products built around them help us deal with information overload or
do they only make things worse?

We (hereby) pose strategic design experiments for ideas, concepts,
products, services, and environments with regards to new ways of
information moderation, transformation, curation, and publishing by
means of (generative) AI.

Part of the experiments is to critically look at the (potential)
positive and negative impact of such technologies and ideas on
democracy, healthcare, education, journalism, arts and cultural
exchange, museums, publishing, built environments, and crossovers.

**This critical view is essential because it is dubious whether new
services such as generative AI are really making us more creative, more
productive, smarter, happier, work better as a society.**

Currently, we are pursuing projects that deal with:

-   enticing intellectual and creative pursuits
-   moderating info-overload instead of contributing
-   mediating people using virtual representatives

## The Assignment

### Problem

In today\'s information-rich environment, we are often called out to
think out-of-the-box and come up with innovative ideas. Because this is
hard, and we are pressed for time, it is tempting to let an AI generate
ideas for us. While the quality of the generated ideas will no doubt
improve over time, using this technology may offload creative thinking
instead of stimulating it.

We would like to be able to use the strengths of generative AI in
lateral thinking (it can easily make connections between two unrelated
concepts, try it) to genuinely stimulate creative thoughts in the user
instead of taking this meaningful activity away from them. Lateral
thinking involves approaching problems from new and unexpected angles,
often requiring a break from linear thought processes.

### Assignment

Develop a system that enhances lateral thinking by augmenting existing
text (e.g., fiction, non-fiction, articles) with prompts, questions, or
interactive elements designed to stimulate innovative ideas about an
unrelated problem the user is working on.

We\'re trying to stimulate the process that leads to the sudden ideas
you get while taking a shower or a walk or doing another unrelated
activity.

To do this you need to use new LLM technologies to analyze the text
being consumed by the user to identify key themes, concepts, and
narratives and link them to the unrelated user\'s challenge. Based on
the analysis, generate thought-provoking prompts, questions, or
activities that encourage the user to think about their unrelated
problem in new ways. These prompts should be subtly integrated into the
reading experience without disrupting the flow. Maybe the original text
could be altered?

### Research opportunities

-   Use LLM concepts such as smart prompting, text embeddings, and
    semantic space to analyze the content of texts to identify links
    between an existing texts and unrelated challenges
-   Develop algorithms for augmented reading interventions
-   Explore the psychological and cognitive impact of augmented reading
    on creative problem-solving.
-   Assess the user experience and engagement with the system through
    user testing and feedback.
-   Study the (long-term) effects of using such a system on creativity
    and problem-solving abilities.
-   As with all projects, we invite you to think critically of the
    possible impact of anything you create.

### Expected outcome

Depending on the experience and interest of the student, we envision:

-   an AI-assisted system that takes (part of) the text and generates
    text augmentations based on a pre-determined unrelated user defined
    challenge

-   (web) platform that offers a pleasant reading experience of existing
    reading material (perhaps epubs) and includes the AI-generated
    interventions or augmentations

-   user experience tests and validation whether such a system may
    enhance (long-term) lateral thinking skills

### Guidance

The DPBTSE (The Dead Philosophers Brainstorm To Solve Everything) is
part of the research group Interaction Design (IXD) of Fontys ICT. The
DPBTSE is a combination of a thinktank and a creative lab for the
research group interested in working with the combination of applied:

-   Sciences,
-   Arts (and design),
-   Creativity and Philosophy, to create innovative interactive
    concepts, products, services and experiments.

The project is open ended, meaning we will adapt and respond to
interesting intermediate results more than setting a clear target. We
need adventurous students that are willing to get engaged with the
available researchers, leading to a potentially very rewarding
internship.

Your contacts are:

-   Olaf Janssen, PhD in computational physics, experience in mobile app
    development, web develoment and design, AI and LLMs, game design and
    development.

-   John van Litsenburg, teacher and developer of UXD / IXD strategy,
    concepts, product and services development and 2d / 3d design and of
    societal impact of artificial intelligence. Work and worked for
    Media design, Smart Mobile, Artificial intelligence.

Feel free to contact with any question about the internship assignment.

## Appendix

Current language deconstruction stack, or modality stack (subject to
change):

Core 

1.  Nugget Core Meaning: The fundamental, context-independent meaning of
    the text. This is the most abstract representation of an idea or
    fact, stripped of any emotional, cultural, or stylistic influences. 

2.  Lexical Semantics: The dictionary meanings of words and phrases and
    their relationships (e.g., synonyms, antonyms). This layer adds the
    first level of specificity to the core meaning. 

3.  Syntactic Structures: Grammar and sentence structure that organize
    words and phrases into coherent statements. This modality includes
    understanding parts of speech and their arrangements. 

These first three layers are so elementary linked to the medium text
that together we can represent this as text and preserve the most
neutral specific core meaning. We could also represent it in a knowledge
graph. For this project, we consider layer 3 as the lowest level of our
stack. 

Neutral Linguistic Intent and Quality 

4.  Pragmatic Context: The intended use of language in situational
    contexts, including speech acts (e.g., requests, offers, commands)
    and implicatures, which require an understanding of the speaker's
    intentions and the conversational context. 

5.  Referential Context: The specific entities, locations, times, and
    real-world references mentioned in the text. This layer anchors
    abstract meanings to concrete instances. 

6.  Discourse Coherence: The logical flow and connectivity of ideas
    across sentences and paragraphs, ensuring that the text forms a
    coherent whole rather than disconnected fragments. 

The following three layers add a neutral context to the core message
placing it in a particular application context and whether the message
has a coherent application at all. 

Social Code 

7.  Sentiment and Emotional Tone: The emotional layer of the text, which
    includes sentiments (positive, negative, neutral) and more specific
    emotional states (joy, anger, sadness). 

8.  Cultural References and Symbolism: Implicit and explicit references
    that require cultural knowledge to decode, including idioms,
    proverbs, cultural symbols, and allusions. 

9.  Sociolinguistic Variations: Variations in language use influenced by
    social factors, including dialects, sociolects, and registers. This
    modality reflects the social identity and status of the speaker or
    writer. 

The next three levels add meaning that resonate in a specific social and
cultural context. 

Personal identity 

10. Stylistic Features: The choice of words, tone, and rhetorical
    devices that reflect the author's personal style, genre conventions,
    or the text's intended effect on the reader. 

11. Intertextuality: References to and influences from other texts,
    which require knowledge of those texts to understand fully. 

12. Philosophical and Ideological Stances: The underlying beliefs,
    worldviews, and ideologies that shape the perspective from which the
    text is written. 

13. Temporal and Spatial Contexts: The historical time, geographical
    place, and cultural setting in which the text was produced and is
    interpreted. 

14. Psychological and Personality Traits: Indications of the
    psychological state or personality traits of the speaker or writer,
    as inferred from language use patterns.
