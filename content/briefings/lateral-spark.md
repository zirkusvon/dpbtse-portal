---
title: "Lateral Spark: An augmented reading experience to enhance creativity and problem solving"
date: 2024-05-23T00:00:00+01:00
publishdate: 2024-05-23T00:00:00+01:00
image: "/img/posts/briefings.jpg"
tags: ["briefing"]
type: "post"
summary: "Create a lateral thinking augmentation system for creative professionals to stimulate innovative ideas and solve unrelated problems while consuming existing text."
comments: false
---

**Design challenge:**

> Create a **lateral thinking augmentation system** for **creative
> professionals** to **stimulate innovative ideas and solve unrelated
> problems while consuming existing text**.

The challenge can be approached from different perspectives and can be
adjusted to the IT-Area of interest of the student:

**HBO-i domains:** Software Engineering, User Interaction\
**IT-Areas:** AI engineering, web development (front-end/back-end), UX
design

## Context {#context-1}

The research group Interaction Design (IXD) is interested in the shape,
matter, and power of our relationship with information. In particular,
we see how humans struggle in the face of information overload:

-   Quantity of information (too much or not enough),
-   Quality of information (information density, ease of use of info),
-   Complexity or Simplicity of information (too much or too little).

This struggle is not new ([Kierkegaard,
1840](https://www.historynewsnetwork.org/article/how-to-cope-with-information-overload)),
but the technologies surrounding us are. Do those new technologies and
the products built around them help us deal with information overload or
do they only make things worse?

We (hereby) pose strategic design experiments for ideas, concepts,
products, services, and environments with regards to new ways of
information moderation, transformation, curation, and publishing by
means of (generative) AI.

Part of the experiments is to critically look at the (potential)
positive and negative impact of such technologies and ideas on
democracy, healthcare, education, journalism, arts and cultural
exchange, museums, publishing, built environments, and crossovers.

**This critical view is essential because it is dubious whether new
services such as generative AI are really making us more creative, more
productive, smarter, happier, work better as a society.**

Currently, we are pursuing projects that deal with:

-   enticing intellectual and creative pursuits
-   moderating info-overload instead of contributing
-   mediating people using virtual representatives

## The Assignment

### Problem

We have a proof-of-concept system that can take a text and peel off
layer-by-layer different cultural nuances, world views, perspectives and
emotional sentiments until only a core neutral meaningful text remains.
The system uses the power of Large Language Models (LLMs) to deconstruct
the text, as the mathematical power behind text embeddings, which allows
us to transform the text while keeping its meaning as much the same.

From the core neutral meaningful nugget, we can again build up, layer by
layer, different text representations that convey the same meaning but
in completely different representations.

We can use such a universal translator or transformer of meaning, to
investigate how people from different perspectives or with seemingly
completely different opinions can find common ground in what is
essential. Or, how how to explore how you can repackage your message to
be accepted or understood by a person with a different cultural, social,
or ideological background.

This is a very relevant topic in a world where information is twisted
into false narratives and information is represented in ways to increase
polarization between people instead of bridging the gap.

### Assignment

Depending on the background of the student, the AI-assisted system
behind the universal translator tool can be improved or the student can
work on a novel interface design for such a real-time universal
translator and transformer.

Several challenges to work on:

-   improving the text deconstruction mechanism (see appendix for
    current layers)

-   turning the proof-of-concept into a professional context of vector
    and graph databases

-   a system to apply the translator into mediating different opinions

-   creating an appealing and intuitive interface to transform the text
    real-time

### Research opportunities

-   How to set up the vector and graph databases for storing and
    analysing the huge text transformations

-   Investigate techniques to use the (mathematical) relations of text
    embeddings to bridge the gap between people with different
    perspectives

-   Exploring and prototyping new prompting techniques to improve the
    text de- and re-construction generations using cloud or local LLMs

-   Study user interaction patterns with synthesized content and their
    contributions to the refinement process.

-   As with all projects, we invite you to think critically of the
    possible impact of anything you create.

### Expected outcome

Depending on the experience and interest of the student, we envision:

-   a more professionally designed platform for the universal translator
    prototype

-   a novel and intuitive interface design for the universal translator

-   prototypes or experiments to apply the tool to counter hostility and
    polarization between people, while maintaining a healthy discourse
    between different opinions

### Guidance

The DPBTSE (The Dead Philosophers Brainstorm To Solve Everything) is
part of the research group Interaction Design (IXD) of Fontys ICT. The
DPBTSE is a combination of a thinktank and a creative lab for the
research group interested in working with the combination of applied:

-   Sciences,
-   Arts (and design),
-   Creativity and Philosophy, to create innovative interactive
    concepts, products, services and experiments.

The project is open ended, meaning we will adapt and respond to
interesting intermediate results more than setting a clear target. We
need adventurous students that are willing to get engaged with the
available researchers, leading to a potentially very rewarding
internship.

Your contacts are:

-   Olaf Janssen, PhD in computational physics, experience in mobile app
    development, web develoment and design, AI and LLMs, game design and
    development.

-   John van Litsenburg, teacher and developer of UXD / IXD strategy,
    concepts, product and services development and 2d / 3d design and of
    societal impact of artificial intelligence. Work and worked for
    Media design, Smart Mobile, Artificial intelligence.

Feel free to contact with any question about the internship assignment.
